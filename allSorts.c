/*
 * allSorts.c
 *
 *  Created on: Sep 18, 2015
 *      Author: ravi
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char* argv[]){

	int a[100],i=0,j=0,key=0;
	int n = 100;

	// Generate Random Numbers
	srand(time(NULL));
	for(i=0;i<100;i++){
		a[i] = rand() / 10000000;
	}

	printf("Random Numbers are below.\n");

	// Print Random Numbers
	for(i=0;i<100;i++){
			printf("%d\n",a[i]);
		}

	// Sorting using Insertion Sort.
	for(j=1;j<n;j++){
		key = a[j];
		i = j-1;

		while(i>=0 && a[i]>key){
			a[i+1] = a[i];
			i--;
		}
		a[i+1] = key;
	}

	printf("Sorted Numbers are below.\n");
	// Print Sorted Numbers
	for(i=0;i<100;i++){
		printf("%d\n",a[i]);
	}

	return 0;
}
