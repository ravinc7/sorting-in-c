# README #

This repository is created to host various integer sorting algorithms. The language used is C. Compile instructions are below.

### Compile Instructions ###
a. Please copy the code to your local machine with the name of the file as "allSorts.c"
b. On Ubuntu it can be compiled using "gcc -o output allSorts.c -w"
c. To run the program use "./output"

### Types of sorts available ###
1. Insertion Sort

### Contribution guidelines ###

This repository is for educational purposes and there are no contributors other than the owner of this repository.

Please feel free to use this repository but if you would like to add/del something then please let me know.

### Who do I talk to? ###

Please feel free to contact me via email using ravi.ncstate@gmail.com